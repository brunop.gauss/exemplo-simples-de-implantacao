import pandas as pd
import pickle 
import json
import numpy as np
from scipy import stats
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score, make_scorer
roc_auc_scorer = make_scorer(roc_auc_score)
import matplotlib.pyplot as plt
import seaborn as sns
from datetime import datetime
# %matplotlib inline

def ginidat(dat, label_true='RESP', label_score='score'):
    gs = 2 * roc_auc_score(dat[label_true], dat[label_score]) - 1
    return gs
    
fill_median = {'Age': 28}
var_explic = ['Pclass', 'Sex_male', 'Age', 'SibSp', 'Parch', 'Fare', 'Embarked_C', 'Embarked_Q','Embarked_S']