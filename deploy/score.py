import pandas as pd
from customfunction import *

def argumentparser():
    from argparse import ArgumentParser
    parser = ArgumentParser()
    
    parser.add_argument('tbl_input')
    parser.add_argument('tbl_output')
    parser.add_argument('chunksize', type=int, nargs='?', default = 100)
    
    args = parser.parse_args()
    return args

def main():
    '''implantação do modelo (melhorar)
    '''
    
    args = argumentparser()
    
    #ler modelo
    from sklearn.externals import joblib
    with open('../modelo/mRF.pickle', 'rb') as f:
        m=joblib.load(f)
        
        tbl_input_pool = pd.read_csv(args.tbl_input,
                                    iterator = True,
                                    sep=',',
                                    chunksize=args.chunksize
#                                      ,
#                                     usecols=var_explic
                                    )
        
        #escreve o cabeçalho
        with open(args.tbl_output, 'w') as f:
            f.write('PassengerId,score\n')
            
        t0 = datetime.now()
        n_tot = 0
        with open(args.tbl_output, 'a') as csv_fileout:
            for i, tbl_i, in enumerate(tbl_input_pool):
                ti = datetime.now()
                
                df = tbl_i
                
                df_Sex = pd.get_dummies(df['Sex'], prefix='Sex',drop_first=True)
                df_Embarked = pd.get_dummies(df['Embarked'], prefix='Embarked')
                df = pd.concat([df,df_Sex,df_Embarked], axis=1)
                                
                tbl_i = df
                tbl_i =  tbl_i.fillna(fill_median).fillna('')
                
                # Escorar
                tbl_i['score'] = m.predict_proba(tbl_i[var_explic])[:,1] 
                
                n_rows = tbl_i.shape[0]
                n_tot += n_rows
                
                dt_i = (datetime.now() - ti).total_seconds()
                dt_tot = (datetime.now() - t0).total_seconds()
                
                info_str = f'{n_rows} marcadas em {dt_i:.2f} s / ' +\
                           f'Total: {n_tot} em {dt_tot:.2f} s'
                
                print(info_str)
                
                (tbl_i
                [['PassengerId', 'score']].to_csv(csv_fileout, index=False, header=False))
                
                
if __name__ == '__main__':
    main()