# Pacote de implantação
Este exemplo, visa demonstrar a entrega de uma aplicação em ciência de dados, cujo objetivo é a realização de escoragem de dados.

## Problema tratado
 Na aplicação em questão, estamos tratando o problema do TITANIC, onde o método calcula a chance de sobrviver ao acidente dado diversas características dos tripulantes. 

## Ideia do projeto
A ideia desta aplicação não é desenvolver a melhor solução para este cálculo e sim, por meio de qualquer modelagem demonstrar a criação do pacote de implantação, bem como sua utilização para obtenção dos resultados (escoragem).

## Método utilizado
Vale salientar que foi utilizado um modelo RF, sem nenhum critério de decisão, e até mesmo não foi utilizado nenhum método de tune de hiperparâmetros (Grid/Random search), ou seja, o foco deste trabalho visa somente a criação do pacote de implatação.

## Organização do projeto
O Trabalho está organizado em: 
- /code que representa a pasta de desenvolvimento do modelo; 
- /data que representa a pasta com os arquivos de entrada da escoragem, bem como os arquivos de saída da escoragem; 
- /modelo representa a pasta cujo o modelo encontra-se salvo; e por fim, o mais importante a pasta /deploy, onde temos os arquivos de chamada de execução da escoragem.

## Execução:
1) Para escorar deve-se abrir o gitBash, clonar o projeto e se dirigir a pasta /deploy;
2) Em seguida escrever: python3 score.py ../data/titanic.csv ../data/score.csv, e precionar enter.
3) Neste caso os parâmetros desta aplicação são: o arquivo de execução do modelo. os dados de entrada, e o arquivo de saída.
![modo_exec](modo_exec1.png)
Note que foi configurado um esquema de monitoramento do processo de escoragem, ou seja, é possível notar que o código foi configurado para marcar de 100 em 100 indivíduos, e para cada etapa, temps o tempo de processamento juntamente com o acumulado das marcações e do tempo total acumulado.
No caso em que temos milhares de indivíduos para escorar, é possível monitorar para saber se o processo ta perto do fim, ou saber em que momento o processo quebrou, por exemplo.